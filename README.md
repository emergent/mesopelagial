# Meandering Sines

An ambient composition of meandering sine waves, bell-like sounds and granular sparkles.

These files are the sourcecode to my composition. I do my best to check if the code is working as intended before publication; however, I cannot give any guarantees that the information they contain is complete and accurate, nor can I guarantee that they will produce the intended results on your machine.

Use this code at your own risk.
